package com.softtek.apitristan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTristanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTristanApplication.class, args);
	}

}
